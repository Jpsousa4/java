package Projectile;
import Plot.Plot;
import java.awt.*;
import DoubleScroller.DoubleScroller;
import java.awt.event.*;

/**
 * Hello world!
 */
public final class Projectile 
{
    public Projectile()
    {
        Plot myPlot = new Plot("Projectile Motion", 0, 10, 1, 0, 100, 10);
        Frame myFrame = new Frame("See the button!");
        Panel myPanel = new Panel();
        Button myButton = new Button("Press me!");
        DoubleScroller angleScroller = new DoubleScroller("Launch angle in degrees = ",0,90,1,45);
        myFrame.add(myPanel);
        myPanel.add(myButton);
        myFrame.add(angleScroller);
        myFrame.setLayout(new GridLayout(0,1));
        myFrame.pack();
        myFrame.setVisible(true);
        myButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                myPlot.clearThePlot();
                double angle = angleScroller.getValue();
                drawLaunch(myPlot, 20, 20, .1, 10, .5);
            }
        });
        myFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
    void drawLaunch(Plot myPlot, double yInitialPosition, double angle, double timeStep, double mass, double dragCoefficient) 
    {
        double y = yInitialPosition;
        double vy = yInitialVelocity;
        double t = 0;
        double dt = timeStep;
        final double g = 9.81;
        while(y > 0)
        {
            
            myPlot.addPoint(t, y);
            myPlot.setConnected(true);
            double k1,k2,k3,k4,vk1,vk2,vk3,vk4;
            double vyMid = vy;
            double ay = -g - ((dragCoefficient * Math.abs(vyMid) * vyMid)/mass);
            k1 = ay;
            vk1 = vy;
            vk2= vyMid = vy + dt*k1/2;
            k2 = (-g - ((dragCoefficient * Math.abs(vyMid) * vyMid)/mass));
            vk3= vyMid = vy + dt*k2/2;
            k3 = (-g - ((dragCoefficient * Math.abs(vyMid) * vyMid)/mass));
            vk4= vyMid = vy + dt*k3;
            k4 = (-g - ((dragCoefficient * Math.abs(vyMid) * vyMid)/mass));
            y += dt*(vk1+ 2*vk2+ 2*vk3 + vk4)/6;
            vy += dt*(k1 + 2*k2 + 2*k3 + k4)/6; 
            t += dt;
            myPlot.setConnected(false);
        }
    }
    /**
     * Says hello to the world.
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
        
        new Projectile();
       //new Projectile(myPlot, 20, 20.0, 1, 2, 0,false);
    }
}
