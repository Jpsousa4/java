public class RangeCalc {
    
    RangeCalc(double speed, double angleInDegrees){
        double g, angleInRads, range;
        g = 9.8;                        //SI units.
        angleInRads = angleInDegrees * Math.PI / 180;
        range = 2 * speed * speed * 
            Math.sin(angleInRads) * Math.cos(angleInRads) / g;
            System.out.println("A projectile launched at " + speed + "m/s and " + angleInDegrees + "degrees will have a Range of " + range + " meters");
    }

    public static void main(String[] arg)
    {
        new RangeCalc(2,45); // launch at 20 m/s, 45 degrees
    }
}
