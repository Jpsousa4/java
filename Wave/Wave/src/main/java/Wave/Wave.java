package Wave;
import Plot.Plot;
import java.util.concurrent.TimeUnit;
import java.lang.Math;
/**
 * Hello world!
 */
public final class Wave {
    private Wave() 
    {
        Plot thisWavePlot = new Plot("Title",0,Math.PI*4,Math.PI/4,-1.5,1.5,.5);
        thisWavePlot.setPointSize(0);
        thisWavePlot.setConnected(true);
        for(double i = 0; i < Math.PI * 4 ; i += .05)
        {
            try{TimeUnit.MILLISECONDS.sleep(3);}
            catch(InterruptedException e){}
            thisWavePlot.addPoint(i, sineSum(i,5));
        }
    }

    double sineSum(double radians, int numberOfTerms)
    {
        double y = 0;
        for(int i = 1; i < numberOfTerms+1; i+=2)
        {
            y += Math.sin(radians*(i)) / (double)i;
        }
        return y;
    }

    /**
     * Says hello to the world.
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
    new Wave();
    }
}
