package woofyBunny;
import java.text.DecimalFormat;
//import woofyBunny.Plot;
public class RangeCalc {
    
    RangeCalc(double speed, double angleInDegrees){
        DecimalFormat twoDecimalPlaces = new DecimalFormat("0.00");
        double range = calcRange(speed, angleInDegrees);
            System.out.println(
                "A projectile launched at " + twoDecimalPlaces.format(speed) + "m/s and " + 
                twoDecimalPlaces.format(angleInDegrees) + " degrees will have a Range of " + 
                twoDecimalPlaces.format(range) + " meters");
        Plot myPlot = new Plot("Plot of y vs x",0,90,10,0,50,10);
        myPlot.setConnected(true);
        myPlot.setPointSize(0);
        for(double i = 0; i < 90; i+=.1)
        {
            myPlot.addPoint(i,calcRange(speed,i));
        }
    }

    double calcRange(double speed, double angleInDegrees){
        double g, angleInRads, range;
        g = 9.8;                        //SI units.
        angleInRads = angleInDegrees * Math.PI / 180;
        range = 2 * speed * speed * 
            Math.sin(angleInRads) * Math.cos(angleInRads) / g;
        return range;
    }
    public static void main(String[] arg)
    {
        System.out.println("\n\n\n");
        double speed = 20; //default m/s
        double angle = 45; //default degrees
        try {speed = Double.parseDouble(arg[0]);}
            catch(ArrayIndexOutOfBoundsException e) {}
            catch(NumberFormatException e) {}
        /*    
        try {angle = Double.parseDouble(arg[1]);}
            catch(ArrayIndexOutOfBoundsException e) {}
            catch(NumberFormatException e) {} */
        new RangeCalc(speed, angle);
        System.out.println("\n\n\n");
    }
}


